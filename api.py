import requests
import json

class Api:
    def __init__(self, api_host):
        self.api_host = api_host

    def _get(self, url: str, params: dict={}):
        params.setdefault('format', 'json')
        resp = requests.get(
            self.api_host + url, 
            params=params
        )
        return resp.json()

    def _post(self, url: str, data: dict={}, params: dict={}):
        params.setdefault('format', 'json')
        resp = requests.post(
            self.api_host + url,
            json.dumps(data),
            params=params,
            headers={'Content-Type': 'application/json'}
        )
        return resp.json()

    def _send(self, url: str, content: bytes, params: dict={}):
        params.setdefault('format', 'json')
        return requests.post(
            self.api_host + url,
            files={'photo': content},
            params=params
        ).json()

    def _suggest(self, products, exclude=None):
        if exclude is None:
            exclude = []
        return self._post('api/v1/recipes/suggestions/', {
            'products': products,
            'exclude': exclude
        })

    def get_test_recipes(self):
        return self._get('api/v1/recipes')['results'][:3]

    def get_recipes(self, products, exclude=None):
        return self._suggest(products, exclude)['recipes'][:10]

    def get_recomends(self, products):
        data = self._suggest(products)['suggestions']
        return data

    def get_steps(self, recipe):
        data = self._get(f'api/v1/recipes/{recipe}/steps')
        return sorted(data, key=lambda x: x['step_number'])

    def send_photo(self, content):
        data = self._send('api/v1/foodphoto/', content)
        return [i['name'] for i in data['products']]

    def check_good(self, good):
        return True
