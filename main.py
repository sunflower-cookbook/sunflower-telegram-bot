import os
import sys
import telebot
import requests
import replies as R
from api import Api
from typing import Iterable, Optional, List
from telebot import (
    types,
    util,
)


BOT_API_TOKEN = os.environ.get('BOT_API_TOKEN')
API_HOST = os.environ.get('API_HOST')

if BOT_API_TOKEN is None or API_HOST is None:
    print('BOT_API_TOKEN or API_HOST is not set!', file=sys.stderr)
    sys.exit(1)

bot = telebot.TeleBot(BOT_API_TOKEN)
api = Api(API_HOST)

class UserData:
    def __init__(self, goods: Optional[Iterable[str]]=None):
        self.goods: List[str] = goods or []
        self.excl: List[str] = []
        self.recom: Optional[List[str]] = None

def wrap_handler(func):
    def inner(message: types.Message, *a, **ka):
        if message.text == '/start': 
            return handle_start(message)
        if message.content_type == 'photo':
            return handle_upload(message)
        return func(message, *a, **ka)
    inner.__name__ = func.__name__
    return inner

def smart_join(items):
    if not items:
        return 'Ничего'
    *first, last = items
    if first:
        return f"{', '.join(first)} и {last}"
    return last

def cook_time(t):
    if t == 0:
        return 'чуть-чуть'
    h, m = divmod(t, 60)
    if h * m == 0:
        return "%d %s" % (max(h, m), "минут" if h == 0 else "часов")
    return "%d часов %d минут" % (h, m)

def send_message(chat_id: int, text: str, markup: Optional=None, photo: Optional[str]=None) -> types.Message:
    kwargs = dict(
        reply_markup=markup or types.ReplyKeyboardRemove(),
        parse_mode='HTML',
    )
    if photo is not None:
        try:
            return bot.send_photo(chat_id, photo, text, **kwargs)
        except:
            # Error during sending photo, try again without it
            return send_message(chat_id, text, markup)
    return bot.send_message(chat_id, text, **kwargs)

def send_recipe(chat_id: int, rec):
    rec['ctime'] = cook_time(rec['cooking_time'])
    lines = [
        '<b>{title}</b>',
        '',
        '{description}',
        '',
    ]
    if rec['cooking_time']:
        lines.append(
            '<i>Время готовки: {ctime}</i>'
        )
    if rec['person_count']:
        lines.append(
            '<i>Количество порций: {person_count}</i>'            
        )
    if rec['povarenok_url']:
        lines.append(
            '<a href="{povarenok_url}">Ссылка на рецепт</a>'
        )
    text = '\r\n'.join(map(lambda s: s.format(**rec), lines))
    mk = types.InlineKeyboardMarkup(row_width=1)
    mk.add(
        types.InlineKeyboardButton(
            R.ITEM_WANT,
            callback_data=rec['recipe_id']
        )
    )
    send_message(chat_id, text, markup=mk, photo=rec.get('photo'))

def send_step(chat_id: int, step):
    text = (
        "<b>{step_number})</b> {description}"
    ).format(**step)
    send_message(chat_id, text, photo=step['photo'])

def reply_markup(*options, width=1):
    mk = types.ReplyKeyboardMarkup(row_width=width)
    mk.add(
        *map(types.InlineKeyboardButton, options)
    )
    return mk

def step_finish(chat_id: int, ud: UserData):
    mk = reply_markup(R.ITEM_FINISH, R.ITEM_CHANGE)
    msg = send_message(chat_id, R.FINISH_ASK % smart_join(ud.goods), mk)
    bot.register_next_step_handler(msg, handle_finish, ud, mk)

@wrap_handler
def handle_finish(message: types.Message, ud: UserData, mk):
    if message.text == R.ITEM_FINISH:
        try:
            recipes = api.get_recipes(ud.goods, ud.excl)
        except:
            return send_message(message.chat.id, R.REQ_ERR)
        for i in recipes:
            send_recipe(message.chat.id, i)
        if not recipes:
            send_message(message.chat.id, R.NOT_FOUND)
    elif message.text == R.ITEM_CHANGE:
        return step_changes(message.chat.id, ud)
    else:
        send_message(message.chat.id, R.WRONG_ANS, mk)
        bot.register_next_step_handler(message, handle_recom, ud, mk)

def step_recom(chat_id: int, ud: UserData):
    mk = reply_markup(R.YES, R.NO)
    if ud.recom is None:
        try:
            ud.recom = api.get_recomends(ud.goods)
        except:
            return send_message(chat_id, R.REQ_ERR)
    if not ud.recom:
        return step_finish(chat_id, ud)
    msg = send_message(chat_id, R.ITEM_ASK % ud.recom[0], mk)
    bot.register_next_step_handler(msg, handle_recom, ud, mk)

@wrap_handler
def handle_recom(message: types.Message, ud: UserData, mk):
    good, *ud.recom = ud.recom
    if message.text == R.YES:
        ud.goods.append(good)
        return step_recom(message.chat.id, ud)
    elif message.text == R.NO:
        ud.excl.append(good)
        return step_recom(message.chat.id, ud)
    else:
        send_message(message.chat.id, R.WRONG_ANS, mk)
        bot.register_next_step_handler(message, handle_recom, ud, mk)

def step_add(chat_id: int, ud: UserData):
    msg = send_message(chat_id, R.ADD_ASK)
    bot.register_next_step_handler(msg, handle_add, ud)

@wrap_handler
def handle_add(message: types.Message, ud: UserData):
    good = message.text.capitalize()
    if api.check_good(good):
        ud.goods.append(good)
        return step_changes(message.chat.id, ud)
    else:
        msg = send_message(message.chat.id, R.ITEM_UNKNOWN)
        bot.register_next_step_handler(msg, handle_add, ud)

def step_del(chat_id: int, ud: UserData):
    msg = send_message(chat_id, R.DEL_ASK)
    bot.register_next_step_handler(msg, handle_del, ud)

@wrap_handler
def handle_del(message: types.Message, ud: UserData):
    good = message.text.capitalize()
    if good in ud.goods:
        ud.goods.remove(good)
        return step_changes(message.chat.id, ud)
    else:
        msg = send_message(message.chat.id, R.ITEM_UNKNOWN)
        bot.register_next_step_handler(msg, handle_del, ud)

def step_changes(chat_id: int, ud: UserData):
    mk = reply_markup(R.ITEM_ADD, R.ITEM_DEL, R.ITEM_READY)
    send_message(chat_id, R.FINISH_ASK % smart_join(ud.goods))
    msg = send_message(chat_id, R.CHANGE_ASK, mk)
    bot.register_next_step_handler(msg, handle_changes, ud, mk)

@wrap_handler
def handle_changes(message: types.Message, ud: UserData, mk):
    if message.text == R.ITEM_ADD:
        return step_add(message.chat.id, ud)
    elif message.text == R.ITEM_DEL:
        return step_del(message.chat.id, ud)
    elif message.text == R.ITEM_READY:
        return step_recom(message.chat.id, ud)
    else:
        send_message(message.chat.id, R.WRONG_ANS, mk)
        bot.register_next_step_handler(message, handle_changes, ud, mk)

@wrap_handler
def handle_found(message: types.Message, ud: UserData, mk):
    if message.text == R.PHOTO_OK:
        send_message(message.chat.id, R.FLATTERED)
        return step_recom(message.chat.id, ud)
    elif message.text == R.PHOTO_BAD:
        return step_changes(message.chat.id, ud)
    else:
        send_message(message.chat.id, R.WRONG_ANS, mk)
        bot.register_next_step_handler(message, handle_found, ud, mk)

@bot.message_handler(content_types=['photo'])
def handle_upload(message: types.Message):
    bot.send_message(message.chat.id, R.PHOTO_PROC)
    file_info = bot.get_file(message.photo[-1].file_id)
    file_url = f"https://api.telegram.org/file/bot{BOT_API_TOKEN}/{file_info.file_path}"
    file = requests.get(file_url)
    try:
        goods = api.send_photo(file.content)
    except:
        return send_message(message.chat.id, R.REQ_ERR)
    mk = reply_markup(R.PHOTO_OK, R.PHOTO_BAD)
    msg = send_message(
        message.chat.id, R.ITEM_FOUND % smart_join(goods), markup=mk    
    )
    bot.register_next_step_handler(msg, handle_found, UserData(goods), mk)

@bot.message_handler(['phrases'])
def handle_phrases(message: types.Message):
    phrases = [getattr(R, i) for i in dir(R) if not i.startswith('_')]
    for i in phrases:
        bot.send_message(message.chat.id, i)

@bot.message_handler(['start'])
def handle_start(message: types.Message):
    send_message(message.chat.id, R.PHOTO_ASK)

@bot.message_handler(['test'])
def handle_test(message: types.Message):
    try:
        recipes = api.get_test_recipes()
    except:
        return send_message(message.chat.id, R.REQ_ERR)
    for rec in recipes:
        send_recipe(message.chat.id, rec)

@bot.callback_query_handler(lambda msg: True)
def handle_query(data: types.CallbackQuery):
    bot.answer_callback_query(data.id)
    try:
        steps = api.get_steps(data.data)
    except:
        return send_message(data.message.chat.id, R.REQ_ERR)
    for st in steps:
        send_step(data.message.chat.id, st)

@bot.message_handler()
def handle_default(message: types.Message):
    bot.send_message(message.chat.id, R.WTF)

bot.polling()
