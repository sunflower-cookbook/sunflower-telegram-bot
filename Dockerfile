FROM python:3.7-slim

COPY ./Pipfile      /bot/Pipfile
COPY ./Pipfile.lock /bot/Pipfile.lock

WORKDIR /bot

RUN pip install pipenv && pipenv install --system

COPY . /bot

ENTRYPOINT ["python", "main.py"]
